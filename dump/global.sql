-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.25 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table castor.global
DROP TABLE IF EXISTS `global`;
CREATE TABLE IF NOT EXISTS `global` (
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- Dumping data for table castor.global: ~42 rows (approximately)
DELETE FROM `global`;
/*!40000 ALTER TABLE `global` DISABLE KEYS */;
INSERT INTO `global` (`key`, `value`, `createdAt`, `updatedAt`) VALUES
	('3D_VIEWER_URL', 'https://new-heatmap-test.s3.eu-central-1.amazonaws.com/index.html', NULL, NULL),
	('ALLOWED_ASSEMBLY_FILE_TYPES_KEY', '[".step", ".stp", ".x_t",".sldasm",".prt"]', NULL, NULL),
	('ALLOWED_FILE_TYPES_KEY', '[".step", ".stp", ".x_t", ".stl",".sldprt",".sldasm",".prt",".catpart",".catproduct", ".iges", ".igs"]', NULL, NULL),
	('ALLOWED_PART_FILE_TYPES_KEY', '[".step", ".stp", ".x_t", ".stl",".sldprt",".prt"]', NULL, NULL),
	('BEST_MATCH_DEFAULT_FILTERS', '{}', NULL, NULL),
	('BIG_PROJECT_SIZE_KEY', '100000000', NULL, NULL),
	('CALCULATION_SETTINGS_GLOBAL_KEY', '{"productionPriceMarkup":0.1,"machineLifetime":5,"costOfCapital":0.05,"machineUptime":0.95,"hoursPerDay":18,"daysPerWeek":5,"buildPrepTime":0.5,"firstTimeBuildPreparation":0.5,"subsequentBuildPreparation":0.5,"buildSetupTimeByOperator":0.5,"buildRemovalTimeByOperator":0.5,"engineerHourCost":70,"operatorHourCost":40,"technicianHourCost":20,"supervisionDuringBuildPercentOperatorTime":0.1,"consumableCostPerHour":0.3,"fteForBuildExchange":1,"fullTrayAssumption":1,"orderFees":15,"additionalOperatingCost":0.3,"consumableCostPerBuild":0,"marginBetweenPartsOnTray":5,"costAmountFactorPercent":0.039,"electricityCost":0.0916,"servicePackageCost":70000,"trayWeightLimitation":1,"mold":{"DFMCosts":5000,"moldMaintenanceCost":5},"cnc":{"camExistence":false,"accuracy":false,"complexity":true,"cncProgrammingPricePerHour":50,"operationCostPerHour":50}}', NULL, NULL),
	('CASTING_PRODUCTION_SETTINGS', '{"productionPriceMarkup":0.1,"machineLifetime":6,"machineUptime":0.95,"defectRate":0.05,"materialPriceMarkup":0.25,"castMachineMaintenanceCost": 0.05,"machineInstallationAndInfrastructure":0.1,"costPerGJ": 17,"dieCastingEnergyPerKg":14.9,"manualTrimmingProductionAdditionalCost":0.3,"machineTrimmingProductionAdditionalCost":0.15,"manualTrimmingToolingAdditionalCost":0,"machineTrimmingToolingAdditionalCost":0.2,"manualTrimmingMaterialAdditionalCost":0.05,"machineTrimmingMaterialAdditionalCost":0,"manualToMachineTrimmingThreshold":50,"complexityFactor":1}', NULL, NULL),
	('CUSTOM_CONFIGURATION_PROPERTIES', '{\r  	"name": "MJF Deal",\r      "printer": 1,\r      "printerMaterial": 132,\r      "material": 132,\r      "filters": {},\r      "priorities": {},\r      "wallThicknessTestInMM": 0.8,\r      "includeSupplyChainCosts":false\r  }', NULL, NULL),
	('DEFAULT_CUSTOM_CNC_SETTINGS_KEY', '{"customsTaxes":8.5,"domesticShipment":2.05,"profitMargin":25}', NULL, NULL),
	('DEFAULT_SERVICE_BUREAU_KEY', 'Shapeways', NULL, NULL),
	('DEFAULT_WALL_THICKNESS_VALUE_MM', '0.8', NULL, NULL),
	('FEA_FORBIDDEN_FILE_TYPES_KEY', '[".stl"]', NULL, NULL),
	('FILTER_PARTS\n_DEFAULT_SELECTION', 'Prinatble', NULL, NULL),
	('GLOBAL_OFF_THE_SHELF_ITEMS_SUBSTRINGS', '["/DIN/","/ISO/","/screw/","/nut/","/ABC/","/SPAX/","/pillar/","/guide bush/","/Ring/","/scheibe/","/disc/","/Schraube/","/bush/","/Bearings/","/bearing/","/lager/","/bolt/","/bolz/","/flange/","/flansch/","/seal/","/dichtung/","/fitting/","/formstück/","/formstueck/","/pipe bend/","/rohrbogen/","/reducer/","/reduzierstück/","/reduzierstueck/","/T-piece/","/T-Stück/","/T-Stueck/","/cap/","/kappen/","/pipe/","/rohr/","/shaft/","/welle/","/strap/","/riemen/","/roller/","/wälz/","/washer/","/spring/","/feder/","/mutter/","/pin/","/stift/","/schraub/","/stud/","/chain/","/kette/","/clamp/","/schelle/","/slide/","/gleit/","/^DN\\\\d{1}x\\\\d{1}\\\\s+|\\\\s+DN\\\\d{1}x\\\\d{1}$|\\\\s+DN\\\\d{1}x\\\\d{1}\\\\s+|^DN\\\\d{1}x\\\\d{1}$/","/^DN\\\\d{2}x\\\\d{2}\\\\s+|\\\\s+DN\\\\d{2}x\\\\d{2}$|\\\\s+DN\\\\d{2}x\\\\d{2}\\\\s+|^DN\\\\d{2}x\\\\d{2}$/","/^DN\\\\d{2}x\\\\d{3}\\\\s+|\\\\s+DN\\\\d{2}x\\\\d{3}$|\\\\s+DN\\\\d{2}x\\\\d{3}\\\\s+|^DN\\\\d{2}x\\\\d{3}$/","/^DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}\\\\s+|\\\\s+DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}$|\\\\s+DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}\\\\s+|^DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}$/","/^DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}\\\\s+|\\\\s+DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}$|\\\\s+DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}\\\\s+|^DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}$/","/^DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}\\\\s+|\\\\s+DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}$|\\\\s+DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}\\\\s+|^DN\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}$/","/^M\\\\d{1}x\\\\d{1}\\\\s+|\\\\s+M\\\\d{1}x\\\\d{1}$  |  \\\\s+M\\\\d{1}x\\\\d{1}\\\\s+  |  ^M\\\\d{1}x\\\\d{1}$/","/^M\\\\d{2}x\\\\d{2}\\\\s+|\\\\s+M\\\\d{2}x\\\\d{2}$|\\\\s+M\\\\d{2}x\\\\d{2}\\\\s+|^M\\\\d{2}x\\\\d{2}$/","/^M\\\\d{2}x\\\\d{3}\\\\s+|\\\\s+M\\\\d{2}x\\\\d{3}$|\\\\s+M\\\\d{2}x\\\\d{3}\\\\s+|^M\\\\d{2}x\\\\d{3}$/","/^M\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}\\\\s+|\\\\s+M\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}$|\\\\s+M\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}\\\\s+|^M\\\\d{1}\\\\.\\\\d{1}x\\\\d{1}$/","/^M\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}\\\\s+|\\\\s+M\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}$|\\\\s+M\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}\\\\s+|^M\\\\d{1}\\\\.\\\\d{1}x\\\\d{2}$/","/^M\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}\\\\s+|\\\\s+M\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}$|\\\\s+M\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}\\\\s+|^M\\\\d{1}\\\\.\\\\d{1}x\\\\d{3}$/","/^M\\\\d{1}\\\\s+|\\\\s+M\\\\d{1}$|\\\\s+M\\\\d{1}\\\\s+|^M\\\\d{1}$/","/^m\\\\d{1}\\\\s+|\\\\s+m\\\\d{1}$|\\\\s+m\\\\d{1}\\\\s+|^m\\\\d{1}$/","/^M\\\\d{2}\\\\s+|\\\\s+M\\\\d{2}$|\\\\s+M\\\\d{2}\\\\s+|^M\\\\d{2}$/","/^M\\\\s+\\\\d{2}\\\\s+|\\\\s+M\\\\s+\\\\d{2}$|\\\\s+M\\\\s+\\\\d{2}\\\\s+|^M\\\\s+\\\\d{2}$/","/^M\\\\d{1}.\\\\d{1}.\\\\d{2}\\\\s+|\\\\s+M\\\\d{1}.\\\\d{1}.\\\\d{2}$|\\\\s+M\\\\d{1}.\\\\d{1}.\\\\d{2}\\\\s+|^M\\\\d{1}.\\\\d{1}.\\\\d{2}$/","/^M\\\\d{1}\\\\.\\\\d{1}\\\\s+|\\\\s+M\\\\d{1}\\\\.\\\\d{1}$|\\\\s+M\\\\d{1}\\\\.\\\\d{1}\\\\s+|^M\\\\d{1}\\\\.\\\\d{1}$/","/^M\\\\s+\\\\d{1}\\\\.\\\\d{1}\\\\s+|\\\\s+M\\\\s+\\\\d{1}\\\\.\\\\d{1}$|\\\\s+M\\\\s+\\\\d{1}\\\\.\\\\d{1}\\\\s+|^M\\\\s+\\\\d{1}\\\\.\\\\d{1}$/","/^DN\\\\d{1}\\\\s+|\\\\s+DN\\\\d{1}$|\\\\s+DN\\\\d{1}\\\\s+|^DN\\\\d{1}$/","/^DN\\\\s+\\\\d{1}\\\\s+|\\\\s+DN\\\\s+\\\\d{1}$|\\\\s+DN\\\\s+\\\\d{1}\\\\s+|^DN\\\\s+\\\\d{1}$/","/^PN\\\\d{1}\\\\s+|\\\\s+PN\\\\d{1}$|\\\\s+PN\\\\d{1}\\\\s+|^PN\\\\d{1}$/","/^PN\\\\s+\\\\d{1}\\\\s+|\\\\s+PN\\\\s+\\\\d{1}$|\\\\s+PN\\\\s+\\\\d{1}\\\\s+|^PN\\\\s+\\\\d{1}$/","/^EH\\\\s+22\\\\d{3}\\\\s+|\\\\s+EH\\\\s+22\\\\d{3}$|\\\\s+EH\\\\s+22\\\\d{3}\\\\s+|^EH\\\\s+22\\\\d{3}$/"]', NULL, '2019-11-17 18:09:14'),
	('LAST_RUN_DATE_KEY_PREFIX0.50', '2020-02-19T10:00:19.778Z', '2018-06-20 10:09:52', '2020-02-19 10:00:19'),
	('LAST_RUN_DATE_KEY_PREFIX1.00', '2020-02-19T09:16:30.717Z', '2018-06-20 10:09:52', '2020-02-19 09:16:30'),
	('LAST_RUN_DATE_KEY_PREFIX12.00', '2020-02-18T22:58:10.831Z', '2018-06-21 07:59:09', '2020-02-18 22:58:10'),
	('LAST_RUN_DATE_KEY_PREFIX24.00', '2020-02-19T06:00:00.778Z', '2018-06-28 05:00:00', '2020-02-19 06:00:00'),
	('MAIL_LIST_ADD_PRINTER_REQ_KEY', '["limor@3dcastor.com"]', NULL, NULL),
	('MAIL_LIST_ADMINS_KEY', '["limor@3dcastor.com"]', NULL, NULL),
	('MAIL_LIST_MULTIPLE_PARTS_ONE_REQ_KEY', '["limor@3dcastor.com"]', NULL, NULL),
	('MAIL_LIST_SERVICE_IT', '["elad@3dcastor.com", "vladimir@3dcastor.com", "michael@3dcastor.com", "lidor@3dcastor.com", "limor@3dcastor.com", "nir@3dcastor.com"]', NULL, NULL),
	('MAIL_LIST_WARNING_FROZEN_PROJECT_KEY', '["limor@3dcastor.com"]', NULL, NULL),
	('OPEN_EXCHANGE_RATES', '{"AED":3.673,"AFN":77.000002,"ALL":105.45,"AMD":481.616228,"ANG":1.795568,"AOA":589.02,"ARS":73.0453,"AUD":1.394885,"AWG":1.8,"AZN":1.7025,"BAM":1.661052,"BBD":2,"BDT":84.82625,"BGN":1.658345,"BHD":0.376701,"BIF":1929,"BMD":1,"BND":1.373617,"BOB":6.9071,"BRL":5.4848,"BSD":1,"BTC":0.000086585088,"BTN":74.80902,"BWP":11.713229,"BYN":2.459252,"BZD":2.016298,"CAD":1.324065,"CDF":1958,"CHF":0.911365,"CLF":0.028316,"CLP":792.600189,"CNH":6.932895,"CNY":6.9374,"COP":3759.4,"CRC":595.436665,"CUC":1,"CUP":25.75,"CVE":94.02,"CZK":22.167,"DJF":178.0525,"DKK":6.3155,"DOP":58.63,"DZD":128.566761,"EGP":15.970217,"ERN":15.002887,"ETB":35.425,"EUR":0.848101,"FJD":2.1359,"FKP":0.767086,"GBP":0.767086,"GEL":3.085,"GGP":0.767086,"GHS":5.765,"GIP":0.767086,"GMD":51.8,"GNF":9615,"GTQ":7.701356,"GYD":209.366783,"HKD":7.75101,"HNL":24.8,"HRK":6.360014,"HTG":112.223387,"HUF":292.774798,"IDR":14740.7,"ILS":3.40854,"IMP":0.767086,"INR":74.814799,"IQD":1190,"IRR":42105,"ISK":136.63,"JEP":0.767086,"JMD":148.756413,"JOD":0.709,"JPY":106.8175,"KES":108.447805,"KGS":77.260151,"KHR":4105,"KMF":417.60028,"KPW":900,"KRW":1183.225,"KWD":0.305949,"KYD":0.833577,"KZT":419.46425,"LAK":9095,"LBP":1514.5,"LKR":185.00462,"LRD":199.325061,"LSL":17.46,"LYD":1.37,"MAD":9.239,"MDL":16.598984,"MGA":3800,"MKD":52.328517,"MMK":1371.912651,"MNT":2847.209307,"MOP":7.985579,"MRO":357,"MRU":37.5,"MUR":40.002205,"MVR":15.41,"MWK":737.5,"MXN":22.32057,"MYR":4.1935,"MZN":71.149985,"NAD":17.465,"NGN":381,"NIO":34.55,"NOK":8.936846,"NPR":119.694454,"NZD":1.517929,"OMR":0.384969,"PAB":1,"PEN":3.5686,"PGK":3.51962,"PHP":48.952838,"PKR":168.15,"PLN":3.731332,"PYG":6920.551995,"QAR":3.64075,"RON":4.0987,"RSD":99.73,"RUB":73.6142,"RWF":960,"SAR":3.750058,"SBD":8.24895,"SCR":17.830112,"SDG":55.3,"SEK":8.690508,"SGD":1.37165,"SHP":0.767086,"SLL":9826.321017,"SOS":584,"SRD":7.458,"SSP":130.26,"STD":21292.767074,"STN":20.8625,"SVC":8.752776,"SYP":511.697378,"SZL":17.465,"THB":31.0785,"TJS":10.313176,"TMT":3.51,"TND":2.7465,"TOP":2.283438,"TRY":7.2996,"TTD":6.765099,"TWD":29.373286,"TZS":2325.719,"UAH":27.523762,"UGX":3676.136358,"USD":1,"UYU":42.468849,"UZS":10225,"VEF":248487.642241,"VES":283789,"VND":23150.011346,"VUV":113.806842,"WST":2.62175,"XAF":556.317716,"XAG":0.03863017,"XAU":0.00051608,"XCD":2.70255,"XDR":0.710831,"XOF":556.317716,"XPD":0.00046635,"XPF":101.205357,"XPT":0.00106611,"YER":250.375049,"ZAR":17.423524,"ZMW":18.368176,"ZWL":322}', '2020-05-25 17:04:18', '2020-08-12 18:47:50'),
	('OVERHANGING_THRESHOLDS', '{"OVERHANGING_MIN_THRESHOLD":0.002, "OVERHANGING_MAX_THRESHOLD":0.005}', NULL, NULL),
	('PART_HOLE_CONFIDENCE_THRESHOLD', '0.1', NULL, NULL),
	('PARTS_CREDIT_KEY', '10', NULL, NULL),
	('SMALL_PROJECT_MAX_SIZE', '100000000', NULL, NULL),
	('SOLUTION_CONFIGURATIONS_INITIALIZED', '1', NULL, '2019-11-17 18:09:14'),
	('STRINGS_HASH', 'a3a9d27e918a8e6f4f823adb289e1d28d8486gap42wb01as20b5a36575', NULL, '2020-10-29 17:28:55'),
	('TASK_PRIORITY_RULES', '[{"projectSizeFrom":200,"projectSizeTo":100000000,"priorityScore":1},{"projectSizeFrom":40,"projectSizeTo":200,"priorityScore":3},{"projectSizeFrom":4,"projectSizeTo":40,"priorityScore":5},{"projectSizeFrom":1,"projectSizeTo":4,"priorityScore":7}]', NULL, NULL),
	('TOLERANCES_THRESHOLDS', '{"METAL_MAX_THRESHOLD":0.1, "METAL_MIN_THRESHOLD":0.05,"PLASTIC_MIN_THRESHOLD":0.1}', NULL, NULL),
	('TRIAL_DAYS', '14', NULL, NULL),
	('UNIFICATION_MIN_PART_COUNT', '2', NULL, NULL),
	('UNIT_TYPE_NOT_SUPPORTED_FORMATS', '[".stl"]', NULL, NULL),
	('UPDATED_VERSION', '1.114.0', NULL, NULL),
	('UPLOAD_PROJECT_DEFAULT_MATERIAL_TYPE_SELECTED', 'plastic', NULL, NULL),
	('UPLOAD_PROJECT_DEFAULT_RADIO_BUTTON_SELECTED', NULL, NULL, NULL),
	('WEIGHT_REDUCTION_DEFAULT_SETTINGS', '{"wallThicknessMin":5,"wallThicknessMax":10,"wallThicknessStep":0.2,"elementSizeMin":1.8,"elementSizeMax":-1,"elementSizeStep":0.05}', NULL, NULL),
	('WEIGHT_REDUCTION_MIN_THICKNESS_THRESHOLD', '5', NULL, NULL),
	('WEIGHT_REDUCTION_THRESHOLD', '15', NULL, NULL);
/*!40000 ALTER TABLE `global` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
