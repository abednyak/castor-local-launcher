INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('COST_SAVING', 'en', 'Cost saving');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('TIME_SAVING', 'en', 'Time saving');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('COMPLEX_GEOMETRY', 'en', 'Complex geometry');

INSERT INTO `feature` (`name`, `on`, `description`, `componentId`) VALUES ('geatAQ', '1', 'get a quote button', 'get_a_q');

INSERT INTO `global` (`key`, `value`) VALUES ('TASK_PRIORITY_RULES', '[{"projectSizeFrom":200,"projectSizeTo":100000000,"priorityScore":1},{"projectSizeFrom":40,"projectSizeTo":200,"priorityScore":3},{"projectSizeFrom":4,"projectSizeTo":40,"priorityScore":5},{"projectSizeFrom":1,"projectSizeTo":4,"priorityScore":7}]');
ALTER TABLE `user`
    ADD COLUMN `taskPriorityBonus` TINYINT(1) NULL DEFAULT '0' AFTER `verified`;

ALTER TABLE `machine_part`
	ADD COLUMN `uniqueSolidsCount` INT NULL DEFAULT NULL AFTER `solidsCount`;

ALTER TABLE `machine_part`
	ADD COLUMN `complexityScore` FLOAT NULL DEFAULT NULL AFTER `uniqueSolidsCount`;

UPDATE `string` SET `value`='[{"title":"Printable","value":"printable"},{"title":"Printable with some changes/concessions","value":"borderline"},{"title":"Unprintable","value":"notPrintable"},{"title":"CAD file not received","value":null},{"title":"Off the shelf item","value":"offTheShelf"},{"title":"not suitable for processing","value":"failed"},{"title":"Not cost effective","value":"notCostEffective"},{"title":"Dependency anaslysis failed","value":"dependencyAnalysisFailed"},{"title":"Printable & Cost saving","value":"costSavingPrintable"},{"title":"Updating...","value":"loading"}]' WHERE  `id`=472;
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('UPDATING...', 'en', 'Updating...');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('CONFIGURATION_CREATED_OR_UPDATED_SUCCESFULLY', 'en', 'Configuration updated or created successfully');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('ANALYSIS_FINISHED', 'en', 'Analysis finished');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('ANALYSIS_FINISHED_TEXT', 'en', 'Analysis finished, please click OK to update this page.');
UPDATE `string` SET `value`='Updating our analysis to fit your request, this may take few minutes' WHERE  `name`='CONFIGURATION_CALCULATION_LONG_TIME' and language = 'en';


INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('PART_CONSOLIDATION_OPPORTUNITIES', 'en', 'Parts Consolidation Opportunities');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('PART_CONSOLIDATION_OPPORTUNITIES_HELPER', 'en', 'We automatically identify opportunities for clusters of parts that can be consolidated into a single part and check for its 3D printability. Consolidating multiple parts into one, reduces the complexity of an assembly, has the potential to save assembly time, reduce inventory management costs and can potentially improve mechanical properties.');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('WITH_SAME_MATERIAL', 'en', 'with the same material type');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('WITH_ANY_MATERIAL', 'en', 'with any material type');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('WITH_SAME_MATERIAL', 'en', 'with the same material type');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('ANY_MATERIAL', 'en', 'any material');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('SAME_MATERIAL', 'en', 'same material');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('CONSOLIDATION_WILL_BE_OVERWRITTEN', 'en', 'All current consolidation results will be overwritten');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('CONSOLIDATION_DELETE_RESULTS', 'en', 'delete current results and start new search');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('CONSOLIDATION_KEEP_RESULTS', 'en', 'keep current results and cancel the search');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('SEARCH_AGAIN', 'en', 'Search again');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('CONSOLIDATION_WILL_BE_REMOVED', 'en', 'This part and all related consolidation results will be delete');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('CONSOLIDATION_DELETE_PART', 'en', 'delete this part and all related consolidation results');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('CONSOLIDATION_KEEP_PART', 'en', 'keep this part and all related consolidation results');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('COMBINE_AROUND_THIS_PART', 'en', 'Combine around this part');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('SEARCH_AGAIN_FOR_RESULT', 'en', 'Search again for updated results');

ALTER TABLE `project`
    ADD COLUMN `recalculateClusters` TINYINT(1) NULL DEFAULT NULL AFTER `recalculateSolutions`;
ALTER TABLE `machine_part`
    ADD COLUMN `canCombine` TINYINT(1) NULL DEFAULT NULL AFTER `partComplexityScore`;

INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('SEARCH_WITHIN_SAME_CATEGORY', 'en', 'search within same material category');
INSERT INTO  `string` (`name`, `language`, `value`) VALUES ('COMBINE_AROUND_THIS_PART_INFO', 'en', '
        We will automatically identify opportunities for clusters of parts that can be combined into a single part and check for its 3D printability.
        Combining multiple parts into one, reduces the complexity of an assembly, has the potential to save assembly time, reduce inventory management costs and can potentially improve mechanical properties.
        Results will be available in the project page view, in the "Advanced features section"
');

ALTER TABLE `machine_part` DROP COLUMN `clusterStructure`;
ALTER TABLE `machine_part`
    ADD COLUMN `clusterId` varchar(256) NULL DEFAULT NULL AFTER `parentAssemblyId`;
ALTER TABLE `machine_part`
	CHANGE COLUMN `clusterId` `clusterId` VARCHAR(256) NULL DEFAULT NULL COMMENT 'temp field' AFTER `compositionSetParts`;


ALTER TABLE `user_customization_settings`
	ADD COLUMN `machining` BOOLEAN DEFAULT TRUE AFTER `id`;

INSERT INTO `string` (`name`, `language`, `value`) VALUES ('CUSTOMIZE_MACHINING_TITLE', 'en', 'Default Post Processes ');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('CUSTOMIZE_MACHINING_EXPLANATION_ARRAY', 'en', '["Please provide your preferences for default post processes to be added to the 3D printed parts cost"]');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_MACHINING_INFO', 'en', 'Cost of machining by CNC will be added to the 3D printed part’s cost. Machining is needed for most metal 3D printed parts in order to achieve tolerances less than +/-0.1 mm, or to achieve the requires surface finish or for other purposes.');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_DYEING_INFO', 'en', 'During the dyeing process the part is immersed in a hot bath with added dye. Using such a bath allows the dye to penetrate all internal and external surfaces. The dye is typically absorbed only to a depth of approximately 0.5mm into the parts surface.');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_POLISHING_INFO', 'en', 'Parts are polished in a vibration machine to achieve a smoother surface texture. These machines use small vibrating ceramic chips to gradually erode the outer surface of the part to achieve a polished finish. The polishing process has a minimal effect on the part dimensions, and may result in rounding of sharp edges for example.');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_HEAT_TREATMENT_INFO', 'en', 'Heat treatment will be preformed according to existing standards and/or the manufacturer recommendations');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_DYEING_LABEL', 'en', 'Dyeing');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_POLISHING_LABEL', 'en', 'Polishing');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_HEAT_TREATMENT_LABEL', 'en', 'Heat treatment');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('POST_PROCESSES_MACHINING_LABEL', 'en', 'Machining');
UPDATE `post_optional` SET `labelName` = 'POST_PROCESSES_MACHINING_LABEL', `info` = 'POST_PROCESSES_MACHINING_INFO' WHERE (`id` = '4');
UPDATE `post_optional` SET `labelName` = 'POST_PROCESSES_DYEING_LABEL', `info` = 'POST_PROCESSES_DYEING_INFO' WHERE (`id` = '1');
UPDATE `post_optional` SET `labelName` = 'POST_PROCESSES_POLISHING_LABEL', `info` = 'POST_PROCESSES_POLISHING_INFO' WHERE (`id` = '2');
UPDATE `post_optional` SET `labelName` = 'POST_PROCESSES_HEAT_TREATMENT_LABEL', `info` = 'POST_PROCESSES_HEAT_TREATMENT_INFO' WHERE (`id` = '3');

INSERT INTO `feature` (`name`, `on`, `description`, `componentId`) VALUES ('customizeUserPostProcesses', '1', 'show toggle button on user setting for turn on machining.', 'customize_user_post_processes');
UPDATE `filter` SET `showAsDefault`='0', `mandatory`='0', `viewOnly`='1', `order`='200000' WHERE  `id`=1;
UPDATE `filter` SET `viewOnly`='1', `order`='200000' WHERE  `id`=3;
UPDATE `all_printer` SET `holeThresholdMM`='0.2' WHERE  `id`=2052;
UPDATE `all_printer` SET `holeThresholdMM`='0.25' WHERE  `id`=2062;
UPDATE `post_processes_availability` SET `ded`='{\r\n "all":false,\r\n "materials_ids":[365]\r\n}' WHERE  `id`=4;

ALTER TABLE `machine_part`
    ADD COLUMN `disregardMaterial` TINYINT(1) NULL DEFAULT NULL AFTER `clusterId`;
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('IMAGES_TAB_HEADER_STRUCTURAL_LIMITATION_CLUSTER', 'en', 'Consolidation Opportunity');
UPDATE `post_processes_availability` SET `slm`='{\r\n "all":false,\r\n "materials_ids":[1,2,3,4,104,105,106,107,140,141,179,181,183,185,187,189,191,118,150,153,212,249,251,328,319,318,288,366]\r\n}' WHERE  `id`=4;

UPDATE `string` SET `value` = 'Weight Reduction Opportunities'  WHERE  `name`='WEIGHT_REDUCTION_OPPORTUNITIES' and language = 'en';
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('WEIGHT_REDUCTION_OPPORTUNITIES_HELPER', 'en', 'Based on your weight reduction target, CASTOR proprietary algorithm automatically identifies weight reduction opportunities by removing extra material from your bulky components and creating lighter part for download');

UPDATE `string` SET `value`='No suitable parts were found to consolidate' WHERE  `name`='COMBINE_MULTIPLE_INTO_ONE_CARD_NO_CLUSTERS_TXT' and language = 'en';
UPDATE `string` SET `value`='No suitable parts were found for weight reduction ' WHERE  `name`='WEIGHT_REDUCTION_NOT_FOUND_TXT' and language = 'en';
INSERT INTO `feature` (`name`, `on`, `description`, `componentId`) VALUES ('applicationSelector', '0', 'show \'application\' selector in upload project page', 'application_selector');
INSERT INTO `feature` (`name`, `on`, `description`, `componentId`) VALUES ('cadSoftwareInput', '0', 'show \'cad software\' inpute in upload project page', 'cad_software_input');
UPDATE `string` SET `value`='We will automatically identify opportunities for clusters of parts that can be combined into a single part and check for its 3D printability.\r\nCombining multiple parts into one, reduces the complexity of an assembly, has the potential to save assembly time, reduce inventory management costs and can potentially improve mechanical properties.\r\nResults will be available in the project page view, in the "Advanced features section".' WHERE  `name`='COMBINE_AROUND_THIS_PART_INFO' and `language`= 'en';

INSERT INTO `string` (`name`, `language`, `value`) VALUES ('COMBINE_AROUND_THIS_PART_SEARCH_EMPTY', 'en', 'We will search again for part consolidation opportunities, using the updated parts in the projects.');

UPDATE `string` SET `value` = 'Based on your weight reduction target, CASTOR proprietary algorithm automatically identifies weight reduction opportunities by removing extra material from your bulky components and creating lighter part for download.' WHERE  `name`='WEIGHT_REDUCTION_OPPORTUNITIES_HELPER' and language = 'en';
UPDATE `string` SET `value` = 'The request was sent and will be evaluated shortly.\r\nAn Email informing you of results will be sent.' WHERE `name`='COMBINE_MULTIPLE_INTO_ONE_REQ_BODY' and language = 'en';
UPDATE `string` SET `value` = 'The request was sent and will be evaluated shortly.\r\nAn Email informing you of results will be sent.' WHERE `name`='COMBINE_MULTIPLE_INTO_ONE_CARD_REQESTED_TXT' and language = 'en';

-- --------------------------------------------------------
-- Host:                         eks-test-rds.c8fbimggst4x.eu-central-1.rds.amazonaws.com
-- Server version:               8.0.23 - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table castor-test.pages
DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tourSteps` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table castor-test.pages: ~3 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`name`, `description`, `tourSteps`, `id`, `createdAt`, `updatedAt`) VALUES
	('uploadProject', 'The first page of the dashboard', '[{"title":"Upload your first project!","target":"#upload-project-form-title","content":"Upload the CAD files you want to examine for 3D printing. You should also provide a non-3D printed material for comparison, as well as estimated yearly production quantity.","placement":"left-start","noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#sidebar-my-projects","content":"After the project has finished uploading you can see it here.","placement":"right","noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7","width":"300px"}}},{"target":"#upload-form-field--project-name","content":"Give your project a name! Any name can be used. You will receive notifications to your email about this project, using this project name","placement":"right","spotlightClicks":true,"noGradient":true,"spotlightPadding":20,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#upload-form-field--yearly-prod","content":"Please tell CASTOR the estimated yearly production quantity. Based on this estimation, CASTOR will provide the financial break even point of 3D printing compared with traditional manufacturing","placement":"right","spotlightClicks":true,"noGradient":true,"spotlightPadding":20,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#upload-project-materials","content":"Please tell us the traditional (non-3D printed) material you use for this design. This will allow CASTOR to ensure the 3D printed material it offers, will fit your cost and engineering needs","placement":"right","spotlightClicks":true,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#upload-project-dropzone","content":"Click here to upload your files. You can upload full assemblies files or separate parts files","placement":"right","spotlightClicks":true,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":".upload-project--unit-type-field","content":"For STL files, please tell CASTOR the unit type used in your project (mm, cm, feet, inches, etc..)","placement":"right","spotlightClicks":true,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#upload-project-upload-button","content":"CASTOR is now ready to upload your files and automatically process it. Please click on the \'close\' button to finish this “Take-a-Tour” and click on the \'Start uploading\' button to start processing your files.","placement":"right","noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}}]', 1, NULL, NULL),
	('projectAnalysis', 'Project Analysis', '[{"target":"#general-part-statistics","content":"This section shows general statistics about the parts. The statistics provide you with information about the printability of the part.","placement":"bottom-start","disableScrolling":false,"spotlightPadding":30,"offset":30,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#advanced-features","title":"Advanced features","content":"CASTOR provides you with sophisticated algorithms to find opportunities to reduce the weight of a part as well as the ability to combine parts using 3D printing.","placement":"bottom-start","disableScrolling":false,"spotlightPadding":30,"offset":30,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#advanced-features-single-part","title":"Advanced features","content":"CASTOR provides you with sophisticated algorithms to find opportunities to reduce the weight of a part as well as the ability to combine parts using 3D printing.","placement":"top-start","hideBackButton":true,"disableScrolling":false,"spotlightPadding":30,"offset":30,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#grid-list-tile","content":"Click on a part to see 3D printing solutions.","placement":"top","noGradient":true,"hideNextButton":true,"showSkipButton":true,"locale":{"skip":"Close"},"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":".single-part-project--part","content":"Click on the part to see 3D printing solutions.","placement":"bottom","noGradient":true,"hideNextButton":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}}]', 2, NULL, NULL),
	('mainPartAnalysis', 'Main Part Analysis', '[{"target":"#solution-analysis-object","content":"Click on the configuration to see more details.","placement":"top","noGradient":true,"hideNextButton":true,"spotlightPadding":20,"disableOverlayClose":true,"spotlightClicks":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-tabs-title-analysis-results","content":"Geometric Analysis explains the reasons why the part is printable or unprintable.","placement":"top","noGradient":true,"disableScrolling":true,"hideBackButton":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-tabs-title-material-comparison","content":"Material Analysis shows the differences in properties between the traditional material to the 3D-printed material.","placement":"top","noGradient":true,"disableScrolling":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-tabs-title-cost-comparison","content":"Cost Analysis presents the financial break-even point. It allows you to decide whether to prefer 3D printing over traditional manufacturing methods when it saves money.","placement":"top","noGradient":true,"disableScrolling":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-tabs-title-mechanical-analysis","content":"Stress Analysis predicts the 3D printed part behavior under static mechanical stresses using linear elasticity theory. It allows you to decide whether your 3D printed part will hold the mechanical stresses applied to it.","placement":"top","noGradient":true,"disableScrolling":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-configure-button","content":"Click “Configure” to add your specific material requirements.","placement":"top","noGradient":true,"hideNextButton":true,"disableOverlayClose":true,"spotlightClicks":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-material-properties-importance","content":"Change the material properties importance according to your needs.","placement":"top","noGradient":true,"hideBackButton":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-material-properties-filters","content":"Change the material property filters according to your needs.","placement":"top","noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-calculate-button","content":"Recalculate and find the most fitting 3D-printing solution for your needs.","placement":"top","noGradient":true,"hideNextButton":true,"disableScrolling":true,"disableOverlayClose":true,"spotlightClicks":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-3d-button","content":"Click on the 3D-viewer to observe the thin areas of the part.","placement":"right","hideBackButton":true,"noGradient":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}},{"target":"#part-analysis-get-a-quote-button","content":"Get a quote from 3D-printing service bureaus","placement":"bottom","noGradient":true,"hideNextButton":true,"styles":{"options":{"arrowColor":"#b9e5c7"}}}]', 3, NULL, NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;


INSERT INTO `feature` (`name`, `on`, `description`, `componentId`) VALUES ('combineAroundPart', '0', 'consolidate around part request', 'combine_around_part');
UPDATE `feature` SET `on`='1' WHERE  `name`='holesAnalysis';
UPDATE `feature` SET `on`='1' WHERE  `name`='unification';

UPDATE `project` p SET recalculateClusters=1, clusterStatus = null;
DELETE FROM `machine_part` mp WHERE  mp.compositionSetParts IS NOT NULL;

--take all feature table
--take all post process availabilty table



INSERT INTO `string` (`name`, `language`, `value`) VALUES ('ADMIN_PROJECTS_FILTER_NOT_PUBLISHED', 'en', 'Unpublished projects only');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('ADMIN_PROJECTS_FILTER_NOT_QA', 'en', 'Non QA projects only');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('ADMIN_PROJECTS_FILTER_NOT_ADMIN', 'en', 'Non-admin projects only');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('ADMIN_PROJECTS_FILTER_NAME', 'en', 'Filter project');
INSERT INTO  `feature` (`name`, `on`, `description`, `componentId`) VALUES ('customConfiguration', '0', 'generate custom configuration ', 'custom_configuration');
INSERT INTO  `function_strings` (`category`, `functionString`, `explanation`) VALUES ('COST_PER_PART_CUSTOM_SOLUTION', 'partMaterialVolume * 0.00048 * 1.15 * 1000', 'costPerPartForCustomSolution');
INSERT INTO  `global` (`key`, `value`) VALUES ('CUSTOM_CONFIGURATION_PROPERTIES', '{\n 	\"name\": \"MJF Deal\",\n     \"printer\": 1,\n     \"printerMaterial\": 132,\n     \"material\": 132,\n     \"filters\": {},\n     \"priorities\": {},\n     \"wallThicknessTestInMM\": 0.8,\n     \"includeSupplyChainCosts\":true\n }');
INSERT INTO `string` (`name`, `language`, `value`) VALUES ('STANDARD_NAME_DUPLICATE_ERROR_EXPLANATION', 'en', 'This part name is already in use, please use another');
UPDATE solution_benefit
    SET name = 'Cost saving'
    WHERE type = 'cost-saving';

UPDATE solution_benefit
    SET name = 'Time saving'
    WHERE type = 'time-saving';

UPDATE solution_benefit
    SET name = 'Weight reduction'
    WHERE type = 'weight-reduction';
    
UPDATE  `work_estimation_post_processe` SET `material_category_name` = 'Maraging Steels' WHERE (`material_category_name` = 'Maraging Steel');


UPDATE `string` SET `value` = 'Changed orientation'
    WHERE (`name` = 'ORIENTATION_CHANGED_MSG' AND `language` = 'en');
	
UPDATE `global` SET `value`='a3a9d27e918a8e6f4f823adb289e1d28d8486gap42wb01as20b5a36575' WHERE  `key`='STRINGS_HASH';
UPDATE `global` SET `value`='1.114.0' WHERE  `key`='UPDATED_VERSION';
