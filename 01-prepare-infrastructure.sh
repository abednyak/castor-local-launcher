if [ -z "$1" ]; then
   echo 'Need to set random string for vpn cert generation'
  exit 1
else
  cd scripts
  sh ./01-create-utils-docker-compose.sh
  sh ./02-create-vpn-cert.sh "$1"
  sh ./03-run-utils-docker-compose.sh
  sh ./09-clone-all-microservices-repos.sh
  sh ./13-run-notification-manager-utils-docker-compose.sh
fi

