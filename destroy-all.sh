cd ../
sudo rm -R -f .data
sudo rm -R -f authorization
sudo rm -R -f castor-api-gateway
sudo rm -R -f castor-external
sudo rm -R -f castor-node.js-server
sudo rm -R -f castor-react-client
sudo rm -R -f notification-manager
sudo rm -R -f vpn
sudo rm docker-compose.yml
cd castor-local-launcher
sudo rm dump/castor-test-dump.sql
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q) && docker rmi $(docker images -q)