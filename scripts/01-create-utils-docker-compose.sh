cd ../../
sudo bash -c 'echo "
version: '"'3.7'"'
services:
  rabbitmq:
    image: rabbitmq:3.8.3-management
    container_name: 'castor-rabbitmq'
    restart: unless-stopped
    networks:
      - castor
    ports:
      - 5672:5672
      - 15672:15672
    volumes:
      - .data/rabbitmq/data/:/var/lib/rabbitmq/
      - .data/rabbitmq/log/:/var/log/rabbitmq
  mysql8:
    image: mysql:8.0.26
    command: --default-authentication-plugin=mysql_native_password
    restart: unless-stopped
    container_name: 'castor-mysql-8'
    networks:
      - castor
    environment:
      MYSQL_DATABASE: 'castor'
      MYSQL_USER: 'user'
      MYSQL_PASSWORD: 'password'
      MYSQL_ROOT_PASSWORD: 'password'
    ports:
      - '3306:3306'
    expose:
      - '3306'
    volumes:
      - .data/mysql8/data/:/var/lib/mysql
  adminer:
    image: adminer
    restart: unless-stopped
    ports:
      - 8099:8080
networks:
  castor:
    name: castor_network

" >> docker-compose.yml'
