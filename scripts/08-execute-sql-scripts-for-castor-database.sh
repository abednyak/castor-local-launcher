cd ../
cd dump
docker cp mono_staging25.7.21.sql castor-mysql-8:/mono_staging25.7.21.sql
docker cp global.sql castor-mysql-8:/global.sql
docker cp delete-all-users-and-user-subscription-from-castor-db.sql castor-mysql-8:/delete-all-users-and-user-subscription-from-castor-db.sql
docker exec -i castor-mysql-8 mysql -uroot -ppassword -f -D castor < mono_staging25.7.21.sql && echo 'Imported mono_staging25.7.21.sql'
docker exec -i castor-mysql-8 mysql -uroot -ppassword -f -D castor < global.sql && echo 'Imported global.sql'
docker exec -i castor-mysql-8 mysql -uroot -ppassword -f -D castor < delete-all-users-and-user-subscription-from-castor-db.sql && echo 'Imported delete-all-users-and-user-subscription-from-castor-db.sql'
docker exec -i castor-mysql-8 mysql -uroot -ppassword -f -D castor < helpful_sidebar_script.sql && echo 'Imported helpful_sidebar_script.sql'