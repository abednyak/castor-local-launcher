cd ../
rm -R vpn
echo $1
mkdir vpn
cd vpn
mkdir certs
sudo bash -c 'echo "
client
dev tun
proto udp
remote '"$1"'.cvpn-endpoint-0aa8524380f3489e2.prod.clientvpn.eu-central-1.amazonaws.com 443
remote-random-hostname
resolv-retry infinite
nobind
remote-cert-tls server
cipher AES-256-GCM
verb 3
<ca>
-----BEGIN CERTIFICATE-----
MIIDZjCCAk6gAwIBAgIUSwO+yYAIuN//M4iy+1W+2s1FkkgwDQYJKoZIhvcNAQEL
BQAwHzEdMBsGA1UEAwwUYXdzLnZwbi1uZXcuM2RjYXN0b3IwHhcNMjEwMTIxMDkx
NTA3WhcNMzEwMTE5MDkxNTA3WjAfMR0wGwYDVQQDDBRhd3MudnBuLW5ldy4zZGNh
c3RvcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL1oug0PNPp0+22R
QLRzKVGOnyTyrkDsmVuTGBqCgnrJBsHbCbrcKgHiCt/TVkRYIVcGXPOfYc2RCrzW
0Nh9s/W2NLl7xe1lSjj0R0VDfWYujuBlPfjk4GFrrZ2F8wFOa2MugGKPQAlzjU5c
8lbfGh/1YOqhWDRBWM4VJExO9bY2CVN210W145Fkfj3HAc+sxLrVzOcHOwTEcimE
iAXekK58qbUS/goVzNxuPUQ50HKD/dV3NChGueZ9ZPmgrjAyoD3PsrPbRffley3m
pI2+c4tFLe3sqAHcfHMj6tc9F7OtAe8/vrnaoBvoqADGkd1QEm+QrCBLHdXK8sKy
z/mAZdUCAwEAAaOBmTCBljAdBgNVHQ4EFgQUZuir47dKacTGy437wppOyJQR6PEw
WgYDVR0jBFMwUYAUZuir47dKacTGy437wppOyJQR6PGhI6QhMB8xHTAbBgNVBAMM
FGF3cy52cG4tbmV3LjNkY2FzdG9yghRLA77JgAi43/8ziLL7Vb7azUWSSDAMBgNV
HRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQsFAAOCAQEAtIdDDPzZ
/Ui6AEqnZyOYZtz0h0cUNfBucSGIzo/efmnxbx03OubSWDdzMfYU7k7+iqzEMpnj
rjT6fvFI+a793gJ/d0ZzmKFTgbExknqCaQuNNk1C4N3IguAblMqnr9KX5ynJdDQy
ouLa9zXhztItS9rIiKQMHLxzKXwfZvLUza1XRTMo/9WaFt6HikJfE18KzY6pT9Je
3g51z9Bwewf2Vdvp+x0fBu4hh82ccSI1DwOQbAWPH/0VaLZbZVtrJ9panCNIZ+Au
4lQpeaEm8McI7B1fVzRrpX8kiHatgj+ibrSg2cry7DVAs5t0Yv3v6nwUGCmWd4WQ
wOvhS4prR505Hw==
-----END CERTIFICATE-----

</ca>


reneg-sec 0

cert /certs/eu_cert.crt
key /certs/eu_key.key

" >> castor_eu.ovpn'
sudo bash -c 'echo "
client
dev tun
proto tcp
remote '"$1"'.cvpn-endpoint-0cfd36dc140ed727c.prod.clientvpn.us-east-1.amazonaws.com 443
remote-random-hostname
resolv-retry infinite
nobind
remote-cert-tls server
cipher AES-256-GCM
verb 3
<ca>
-----BEGIN CERTIFICATE-----
MIIDZjCCAk6gAwIBAgIUSwO+yYAIuN//M4iy+1W+2s1FkkgwDQYJKoZIhvcNAQEL
BQAwHzEdMBsGA1UEAwwUYXdzLnZwbi1uZXcuM2RjYXN0b3IwHhcNMjEwMTIxMDkx
NTA3WhcNMzEwMTE5MDkxNTA3WjAfMR0wGwYDVQQDDBRhd3MudnBuLW5ldy4zZGNh
c3RvcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL1oug0PNPp0+22R
QLRzKVGOnyTyrkDsmVuTGBqCgnrJBsHbCbrcKgHiCt/TVkRYIVcGXPOfYc2RCrzW
0Nh9s/W2NLl7xe1lSjj0R0VDfWYujuBlPfjk4GFrrZ2F8wFOa2MugGKPQAlzjU5c
8lbfGh/1YOqhWDRBWM4VJExO9bY2CVN210W145Fkfj3HAc+sxLrVzOcHOwTEcimE
iAXekK58qbUS/goVzNxuPUQ50HKD/dV3NChGueZ9ZPmgrjAyoD3PsrPbRffley3m
pI2+c4tFLe3sqAHcfHMj6tc9F7OtAe8/vrnaoBvoqADGkd1QEm+QrCBLHdXK8sKy
z/mAZdUCAwEAAaOBmTCBljAdBgNVHQ4EFgQUZuir47dKacTGy437wppOyJQR6PEw
WgYDVR0jBFMwUYAUZuir47dKacTGy437wppOyJQR6PGhI6QhMB8xHTAbBgNVBAMM
FGF3cy52cG4tbmV3LjNkY2FzdG9yghRLA77JgAi43/8ziLL7Vb7azUWSSDAMBgNV
HRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQsFAAOCAQEAtIdDDPzZ
/Ui6AEqnZyOYZtz0h0cUNfBucSGIzo/efmnxbx03OubSWDdzMfYU7k7+iqzEMpnj
rjT6fvFI+a793gJ/d0ZzmKFTgbExknqCaQuNNk1C4N3IguAblMqnr9KX5ynJdDQy
ouLa9zXhztItS9rIiKQMHLxzKXwfZvLUza1XRTMo/9WaFt6HikJfE18KzY6pT9Je
3g51z9Bwewf2Vdvp+x0fBu4hh82ccSI1DwOQbAWPH/0VaLZbZVtrJ9panCNIZ+Au
4lQpeaEm8McI7B1fVzRrpX8kiHatgj+ibrSg2cry7DVAs5t0Yv3v6nwUGCmWd4WQ
wOvhS4prR505Hw==
-----END CERTIFICATE-----

</ca>


reneg-sec 0

cert /certs/us_cert.crt
key /certs/us_key.key

" >> castor_us.ovpn'
cd certs
sudo bash -c 'echo "
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            f4:05:c8:03:e2:78:ef:31:9b:c5:60:78:bd:50:8a:9b
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=aws.vpn-new.3dcastor
        Validity
            Not Before: Jan 21 09:17:49 2021 GMT
            Not After : Apr 26 09:17:49 2023 GMT
        Subject: CN=client2.eu-central-1.3dcastor
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:b3:68:4e:54:dd:b1:65:5f:8a:0d:92:1f:f9:78:
                    50:77:93:65:7b:08:a8:9e:6d:36:56:59:70:06:14:
                    34:40:58:c4:12:46:08:ad:08:16:a6:cd:7a:5d:50:
                    ad:96:35:5e:b5:76:f3:ed:81:5d:cd:99:08:f7:e0:
                    9f:15:45:6f:bd:d2:61:7b:aa:9f:08:77:10:99:8f:
                    67:5e:01:e2:3e:1f:70:55:19:77:f2:9a:a3:62:cc:
                    13:e3:69:e5:6c:ff:59:6c:80:2b:ac:db:8b:43:ef:
                    5e:55:b0:77:6a:a5:32:ef:91:11:98:0f:67:06:be:
                    41:bc:4e:b3:df:63:5a:27:25:70:a4:5d:61:58:32:
                    7b:cc:b0:0a:b4:ad:22:55:b5:78:0f:e7:26:0a:00:
                    58:39:b2:c5:e4:99:a9:c5:ba:6d:f0:5d:b3:50:d3:
                    b0:aa:18:0c:21:e2:b5:d5:ff:58:93:56:89:10:3a:
                    7b:45:06:01:45:3e:e5:7d:3f:e0:b4:1a:f8:10:c5:
                    84:eb:a7:bf:11:16:b7:60:75:2c:40:98:3e:97:47:
                    66:9d:be:a4:22:56:46:15:a3:ad:12:10:29:c4:d6:
                    36:31:6e:e7:c0:ea:56:b3:69:3a:41:ec:f8:24:9f:
                    b4:ce:fe:d3:55:06:0b:94:af:64:2b:ff:87:8e:0d:
                    f1:eb
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Subject Key Identifier:
                DB:02:AD:35:20:89:BD:A3:9F:7E:5E:9A:B7:3C:87:47:EF:F9:13:56
            X509v3 Authority Key Identifier:
                keyid:66:E8:AB:E3:B7:4A:69:C4:C6:CB:8D:FB:C2:9A:4E:C8:94:11:E8:F1
                DirName:/CN=aws.vpn-new.3dcastor
                serial:4B:03:BE:C9:80:08:B8:DF:FF:33:88:B2:FB:55:BE:DA:CD:45:92:48

            X509v3 Extended Key Usage:
                TLS Web Client Authentication
            X509v3 Key Usage:
                Digital Signature
    Signature Algorithm: sha256WithRSAEncryption
         27:7e:2c:4c:ec:89:e1:9b:13:d6:f9:01:db:42:60:5d:b9:f6:
         8b:81:bd:bc:28:1c:d8:1d:b9:eb:00:a8:41:a0:42:eb:51:5f:
         f9:59:a5:c4:a4:08:c7:fd:8b:a9:1f:79:d8:c0:1b:16:d6:6d:
         c8:22:c4:9a:6a:d3:69:a7:56:80:d4:b9:8c:d4:b2:c2:45:80:
         f2:7f:48:a2:33:88:37:77:a5:49:8c:53:60:c5:d5:49:fa:71:
         05:3c:82:d4:34:3f:6b:37:0a:f3:52:75:e1:bd:4e:1f:5d:23:
         1a:c7:ec:61:c1:eb:f2:4b:69:f5:33:e1:bc:95:ff:2d:50:76:
         36:a8:49:eb:59:03:98:a5:5b:f8:1d:6b:a3:2b:06:03:60:62:
         fe:04:51:90:22:94:7a:82:3a:f0:50:f6:e2:8e:14:fb:0c:0d:
         f9:1a:b2:dd:75:96:a8:1b:53:37:c9:06:50:0d:a8:d7:45:4c:
         02:d0:8b:76:b8:89:f2:b5:2b:aa:c7:59:71:43:33:64:87:2d:
         64:04:ec:71:5f:4d:f4:35:35:18:2e:7c:a8:ef:dd:0f:44:07:
         a7:63:53:d5:0b:29:6e:a1:c4:56:91:81:62:a7:28:bd:71:f6:
         9e:81:0b:6b:93:18:52:cc:17:c5:ae:5a:23:85:8b:e5:f3:4d:
         39:a8:ca:6a
-----BEGIN CERTIFICATE-----
MIIDfjCCAmagAwIBAgIRAPQFyAPieO8xm8VgeL1QipswDQYJKoZIhvcNAQELBQAw
HzEdMBsGA1UEAwwUYXdzLnZwbi1uZXcuM2RjYXN0b3IwHhcNMjEwMTIxMDkxNzQ5
WhcNMjMwNDI2MDkxNzQ5WjAoMSYwJAYDVQQDDB1jbGllbnQyLmV1LWNlbnRyYWwt
MS4zZGNhc3RvcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALNoTlTd
sWVfig2SH/l4UHeTZXsIqJ5tNlZZcAYUNEBYxBJGCK0IFqbNel1QrZY1XrV28+2B
Xc2ZCPfgnxVFb73SYXuqnwh3EJmPZ14B4j4fcFUZd/Kao2LME+Np5Wz/WWyAK6zb
i0PvXlWwd2qlMu+REZgPZwa+QbxOs99jWiclcKRdYVgye8ywCrStIlW1eA/nJgoA
WDmyxeSZqcW6bfBds1DTsKoYDCHitdX/WJNWiRA6e0UGAUU+5X0/4LQa+BDFhOun
vxEWt2B1LECYPpdHZp2+pCJWRhWjrRIQKcTWNjFu58DqVrNpOkHs+CSftM7+01UG
C5SvZCv/h44N8esCAwEAAaOBqzCBqDAJBgNVHRMEAjAAMB0GA1UdDgQWBBTbAq01
IIm9o59+Xpq3PIdH7/kTVjBaBgNVHSMEUzBRgBRm6Kvjt0ppxMbLjfvCmk7IlBHo
8aEjpCEwHzEdMBsGA1UEAwwUYXdzLnZwbi1uZXcuM2RjYXN0b3KCFEsDvsmACLjf
/zOIsvtVvtrNRZJIMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQEAwIHgDAN
BgkqhkiG9w0BAQsFAAOCAQEAJ34sTOyJ4ZsT1vkB20JgXbn2i4G9vCgc2B256wCo
QaBC61Ff+VmlxKQIx/2LqR952MAbFtZtyCLEmmrTaadWgNS5jNSywkWA8n9IojOI
N3elSYxTYMXVSfpxBTyC1DQ/azcK81J14b1OH10jGsfsYcHr8ktp9TPhvJX/LVB2
NqhJ61kDmKVb+B1roysGA2Bi/gRRkCKUeoI68FD24o4U+wwN+Rqy3XWWqBtTN8kG
UA2o10VMAtCLdriJ8rUrqsdZcUMzZIctZATscV9N9DU1GC58qO/dD0QHp2NT1Qsp
bqHEVpGBYqcovXH2noELa5MYUswXxa5aI4WL5fNNOajKag==
-----END CERTIFICATE-----
" >> eu_cert.crt'
sudo bash -c 'echo "
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCzaE5U3bFlX4oN
kh/5eFB3k2V7CKiebTZWWXAGFDRAWMQSRgitCBamzXpdUK2WNV61dvPtgV3NmQj3
4J8VRW+90mF7qp8IdxCZj2deAeI+H3BVGXfymqNizBPjaeVs/1lsgCus24tD715V
sHdqpTLvkRGYD2cGvkG8TrPfY1onJXCkXWFYMnvMsAq0rSJVtXgP5yYKAFg5ssXk
manFum3wXbNQ07CqGAwh4rXV/1iTVokQOntFBgFFPuV9P+C0GvgQxYTrp78RFrdg
dSxAmD6XR2advqQiVkYVo60SECnE1jYxbufA6lazaTpB7Pgkn7TO/tNVBguUr2Qr
/4eODfHrAgMBAAECggEAN4Fp2q/MFoT8fT5cTh5/Jr+fgfUn1fwL96MsGXVexRmx
b8XjIUWDXI/t77jqO7K/Xn0y8qQ3lUhnkekYzsfwzlWse0pKcaAErr3DST5j9Lxd
ULbUeWaGbiadcJputM47qGnvGUfDSn1gPjqed4wsuhmzy8vYICn0xIHE/LQMTFew
J0XhDUVFK3J5QtDC6YS7jqhwyWgjYXyAYb+KwPFKC5NmOLgZ8vVBtFoCPyyRsyQI
dP+HpF9JXIBI3BQp3l6CwYTYtz/DAJ189wBZn7tCMOMj1wB0nPIJ34G0z1JYxcAz
cWU/N79YxdawqQqQgPMUOwIqnTDjdymAbNRRwNUJIQKBgQDkLa5ki1PHgphyIZRR
e/VIIyq9aqpaFn3Cgh4htViCcX/nRVPCqZofcs8jNtEToTEpQByfp22vY/Em06v+
puCAwQnrqGfykFMEpXa/JTi5WwrH1XWdhnI1sGtva58xGnOWuXcM95ti4h/KK75G
bgQ44cnBIz7AOh5owBLrHRRexwKBgQDJSEvLnvvdPQQxohIgqsmRM4qRtWThlTGo
C/jF0re8xPJHUkaTNZSkfqXL+nRoM1/CyTKoNGkgyzaL8+e+dy3TeqpZUxnko4Ed
fNvF8NaiBSvx7xFgVc9FjjvFpFSwPjIGHwYJysqG3hIn69HzeXQTqXPTbWDdqRYd
BvWRasc/vQKBgQCdU43xz0sVvcrkrph4UnK9qVm3vor0kJtneJXUAhij4CQjTt7A
0nXtBxkhG5Ou6C5gVc8mmad8DfnhHoNxkpgqzm7LqJL7YV2MT5X/l2d0Dfzx6hEt
atzmfdylt1vM3IMkQn5+V18xH95xZBXOs95/ZdbZLzb+DVSYPw8MQ0GfUwKBgEe7
mCeNFT2XTTNTCxrRQtyYfb3H45fxT60P9yypGAoSNjXswHIeGkc1S2kSbQHlrS8B
/Ses5fpl8Z6W4fQjDRXdFXCYCRZOnukfYkN8vUEojqy+AndOnXuwYOTi17unrrQ3
wjMjLmI6KkZH6J7YneGT9k9XH7pfVB4sKMAWScQRAoGAWXvugzErw8a5tNpHJjGX
9+1eSiTTURQJY1sut+SQZvTrUE/l5V9mo1fbKmQ+VcnIjdbRIYfpdq1fHrjFP71K
C3dFOplovE73yL+V5E23p8WLZg+AdbMw11O7zA3v9CNWNN+T0jKcIcP+cBcxNAYn
hnAez7W446LuroMRVfYDGsg=
-----END PRIVATE KEY-----
" >> eu_key.key'
sudo bash -c 'echo "
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            e2:77:b2:f0:f7:20:73:e4:42:2f:aa:f7:15:d6:4b:d9
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=aws.vpn-new.3dcastor
        Validity
            Not Before: Jan 21 09:17:20 2021 GMT
            Not After : Apr 26 09:17:20 2023 GMT
        Subject: CN=client1.us-east-1.3dcastor
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:a3:10:94:fc:04:e4:a8:ae:bc:2e:f3:13:ba:ba:
                    e3:15:36:42:17:27:35:41:c3:3f:5c:0a:21:ba:b1:
                    d1:01:2e:fa:86:1e:20:48:52:c7:58:8d:0c:76:3f:
                    a5:b3:dc:8d:38:73:17:06:af:36:e2:fa:7b:ec:b0:
                    e8:a5:7b:a9:e5:b0:77:f6:03:6d:91:19:04:21:8a:
                    cc:62:56:59:c9:c9:44:91:c6:2f:89:7e:1c:12:b9:
                    f4:63:2b:ac:b5:ff:0f:15:ec:7a:cc:ac:af:12:b4:
                    fc:d0:53:bc:76:dc:5b:31:c6:37:19:90:2b:70:36:
                    72:01:28:44:f1:0e:1d:ed:e6:92:cd:5f:a6:fc:01:
                    e8:9d:73:f0:db:31:87:dc:c3:3d:d3:ae:88:cd:79:
                    e9:94:fa:99:df:2d:8f:92:a5:0f:34:84:5a:b2:51:
                    68:69:e1:a2:d1:95:92:41:ef:6e:e6:0b:1b:58:20:
                    92:8a:8f:f4:89:e9:6e:bc:ff:bb:e7:0e:54:f9:1c:
                    a5:9b:50:4f:40:d3:df:89:70:91:3d:68:91:1b:b1:
                    9a:58:dd:3c:c6:fd:9c:21:e1:8d:a7:78:b6:25:d6:
                    ce:49:ff:5a:ed:9e:2a:42:c7:55:1c:34:09:95:a2:
                    82:98:cd:db:0a:e7:b5:1e:93:ee:b7:5b:c2:18:98:
                    fb:0d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Subject Key Identifier:
                28:5D:6A:28:5E:9A:98:75:70:89:67:11:F2:63:7F:CD:7B:DF:BA:D1
            X509v3 Authority Key Identifier:
                keyid:66:E8:AB:E3:B7:4A:69:C4:C6:CB:8D:FB:C2:9A:4E:C8:94:11:E8:F1
                DirName:/CN=aws.vpn-new.3dcastor
                serial:4B:03:BE:C9:80:08:B8:DF:FF:33:88:B2:FB:55:BE:DA:CD:45:92:48

            X509v3 Extended Key Usage:
                TLS Web Client Authentication
            X509v3 Key Usage:
                Digital Signature
    Signature Algorithm: sha256WithRSAEncryption
         5c:c5:58:2e:ff:b5:62:57:ad:54:c4:ba:ec:de:ce:c5:ca:53:
         d8:b6:a1:da:4d:a3:68:38:17:df:c5:f0:bb:59:19:99:3d:52:
         ea:b3:b1:10:13:a3:48:29:1e:59:53:44:ed:1e:7e:b1:ad:e6:
         fb:c5:6c:47:96:77:7f:89:cf:ed:2c:dd:7c:29:43:56:1f:f6:
         4f:52:b5:ac:81:ae:7c:f5:33:a8:f7:3d:dc:11:38:5b:28:c2:
         90:80:65:05:5b:9b:96:ce:15:9f:e4:a9:b9:1f:4c:1a:20:86:
         86:f3:26:c5:01:db:b5:8e:aa:dd:6d:85:12:ea:02:02:45:10:
         75:f5:12:33:8e:18:22:5b:3f:40:ef:ac:60:02:11:21:2a:fe:
         4e:af:ab:5c:6c:04:7b:76:9b:a8:24:1b:d1:8a:67:c3:7a:a2:
         44:01:95:ea:ab:da:bb:bd:66:41:55:9d:f0:c6:f0:09:75:65:
         79:1d:25:b4:0d:94:2a:71:fb:45:fc:a8:db:01:5f:dc:f0:e0:
         27:f2:6b:fa:74:2e:52:58:5a:da:7b:30:88:de:61:a6:6b:8b:
         2f:c8:59:17:5e:62:60:1a:60:ee:20:95:2e:47:06:b4:bc:71:
         03:fa:64:b3:ad:30:38:9f:00:c4:e3:03:32:88:61:ef:9e:e1:
         94:a8:7d:fb
-----BEGIN CERTIFICATE-----
MIIDezCCAmOgAwIBAgIRAOJ3svD3IHPkQi+q9xXWS9kwDQYJKoZIhvcNAQELBQAw
HzEdMBsGA1UEAwwUYXdzLnZwbi1uZXcuM2RjYXN0b3IwHhcNMjEwMTIxMDkxNzIw
WhcNMjMwNDI2MDkxNzIwWjAlMSMwIQYDVQQDDBpjbGllbnQxLnVzLWVhc3QtMS4z
ZGNhc3RvcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKMQlPwE5Kiu
vC7zE7q64xU2QhcnNUHDP1wKIbqx0QEu+oYeIEhSx1iNDHY/pbPcjThzFwavNuL6
e+yw6KV7qeWwd/YDbZEZBCGKzGJWWcnJRJHGL4l+HBK59GMrrLX/DxXsesysrxK0
/NBTvHbcWzHGNxmQK3A2cgEoRPEOHe3mks1fpvwB6J1z8Nsxh9zDPdOuiM156ZT6
md8tj5KlDzSEWrJRaGnhotGVkkHvbuYLG1ggkoqP9Inpbrz/u+cOVPkcpZtQT0DT
34lwkT1okRuxmljdPMb9nCHhjad4tiXWzkn/Wu2eKkLHVRw0CZWigpjN2wrntR6T
7rdbwhiY+w0CAwEAAaOBqzCBqDAJBgNVHRMEAjAAMB0GA1UdDgQWBBQoXWooXpqY
dXCJZxHyY3/Ne9+60TBaBgNVHSMEUzBRgBRm6Kvjt0ppxMbLjfvCmk7IlBHo8aEj
pCEwHzEdMBsGA1UEAwwUYXdzLnZwbi1uZXcuM2RjYXN0b3KCFEsDvsmACLjf/zOI
svtVvtrNRZJIMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQEAwIHgDANBgkq
hkiG9w0BAQsFAAOCAQEAXMVYLv+1YletVMS67N7OxcpT2Lah2k2jaDgX38Xwu1kZ
mT1S6rOxEBOjSCkeWVNE7R5+sa3m+8VsR5Z3f4nP7SzdfClDVh/2T1K1rIGufPUz
qPc93BE4WyjCkIBlBVubls4Vn+SpuR9MGiCGhvMmxQHbtY6q3W2FEuoCAkUQdfUS
M44YIls/QO+sYAIRISr+Tq+rXGwEe3abqCQb0Ypnw3qiRAGV6qvau71mQVWd8Mbw
CXVleR0ltA2UKnH7Rfyo2wFf3PDgJ/Jr+nQuUlha2nswiN5hpmuLL8hZF15iYBpg
7iCVLkcGtLxxA/pks60wOJ8AxOMDMohh757hlKh9+w==
-----END CERTIFICATE-----
" >> us_cert.crt'
sudo bash -c 'echo "
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCjEJT8BOSorrwu
8xO6uuMVNkIXJzVBwz9cCiG6sdEBLvqGHiBIUsdYjQx2P6Wz3I04cxcGrzbi+nvs
sOile6nlsHf2A22RGQQhisxiVlnJyUSRxi+JfhwSufRjK6y1/w8V7HrMrK8StPzQ
U7x23FsxxjcZkCtwNnIBKETxDh3t5pLNX6b8Aeidc/DbMYfcwz3TrojNeemU+pnf
LY+SpQ80hFqyUWhp4aLRlZJB727mCxtYIJKKj/SJ6W68/7vnDlT5HKWbUE9A09+J
cJE9aJEbsZpY3TzG/Zwh4Y2neLYl1s5J/1rtnipCx1UcNAmVooKYzdsK57Uek+63
W8IYmPsNAgMBAAECggEAYpCdXZCfno6wVTi9Djv1E1dOr6wVdwDMBZhiNvVQvoRl
Djibjtvqf04S4E8snAGOozZ+Dp5OcPize6ibl5ueYKZAfMYprnEOnC/HxxEnCN/N
wMeo6FUAJxmb7W3y4begqMHeoGUpFgesYUicPMSJoxaLK7qHZzahxq1Ort63nJqx
y6JIZ/4QniVwAsm3pd1xFxO2w83y0HbIygpIRymn3Jy3y7aaZwGqfJ+uX6Xodq7B
Mt+b5gs3sc2Ww4vSazLUUHJht9RyykO+a7P2IuquIDEwO+U/5QJtKUc3B0MWlElT
LnJWUYIWrQSVotiVooWM1yYuA10Pro5ggzmk6BgWyQKBgQDP9mECFmZs5TxP5XC8
UdTHJo1877vt2hVINznMHvsLeSRc1oJ6Zi1v8o0PnmueQpI7MwVaBqOfW0VfY+iR
0gfHw7P0YKMgipGo/5Cn9N5Cp50gCfKkNNk6ZlHKYY7YHKstNb6VUhgGsUU/b9qs
E/z3nwuDM1y+1XxQ1PWOdd2aMwKBgQDIuztdPY6bNz26a5Oe0BxJ2ZkNrcLM3MLa
grjZvNTXKm7xYivRqP9sg3yA1WcLOPbuYKHkMlkYklIqTTjIJRiLxAdeOarTf2B3
I9GNjQk4cy4AV/RAifVs1j+qkiSPPOIDcA942Tyl1FwOT1DEZ+s2FN5ejYdmp9Ey
KMyXK7BVvwKBgCIKysZ0tD+PJthNTq8FFjejb/XLqI++60zu/V8UkzPFmNY5pxIy
N63jnoO4AxXc/+MA7xdVjuM/JZP/3so//1YMCyEt0IY7KKzpaNyVkOyIU4gzGK0Y
mZaduTXP0no99ydnbNM0pQozaop/3OPeQcKnHlNTzRLQD5GLysaPNL9RAoGAILuf
d5n1Kkx8zWyDew9WvZg1RCzEqmGJiNZuTFtlndy2kLhAiwTy+gALCntCPWtBoSey
3qJzMvWRXoKPMtwRv6Dszqm6uJh545O8tUd9bG4j3zx4gRm+IsxhP3Brc8wq0wKw
EzXhlO0nQdIfSpfjnEMU6/hKTywoifRrQG+IZsMCgYEAsNP1QzcU3uOvhcyQm2O0
WZVgWBp/yB0/l4queyuNoFP6Z9P0ygO1PXMF4TBv9BLEfMwOde1usJ8tpoh7oUaS
tjnTyz2KvD7P4rXMnFRn/v4zadjwE/d1sTGBp4XT3rjf0J/MANdNB2hm3G9QH0m6
L84WMdh48NVUMLk5UWJOYFA=
-----END PRIVATE KEY-----
" >> us_key.key'